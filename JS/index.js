$(document).ready(function () {
	irArriba();

  $(window).scroll(function(){
    if ($(window).scrollTop() == $(document).height()-$(window).height()){
      $('.ir-arriba').css("color", "#fce69a");
      $('.fa-arrow-up').css("color", "black");
    } else {
      $('.ir-arriba').css("color", "black");
      $('.fa-arrow-up').css("color", "#fce69a");
    }
  });
});

function irArriba() {
	$('.ir-arriba').click(function () {
		$('body,html').animate({ scrollTop: '0px' }, 1000);
	});
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('.ir-arriba').slideDown(600);
		} else {
			$('.ir-arriba').slideUp(600);
		}
	});
}
